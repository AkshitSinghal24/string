function split(str){
    
    let tempstr = str.split(".").map(value => Number(value));
    let check = tempstr.filter(value => isNaN(value));
    if(check.length == 0) return tempstr;
    return [];

}

module.exports = split;