
function giveMonth(str){
    const months = ["invalid",'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    let arr = str.split("/");
    return months[arr[Number(1)]];

}

module.exports = giveMonth;