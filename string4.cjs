function changeCase(obj){
    let arr =[];
    for(const [key,value] of Object.entries(obj)){
        let newstr = value.toLowerCase();
        newstr = newstr[0].toUpperCase() + newstr.slice(1);
        arr.push(newstr);
    }
    return arr.join(" ");
}

module.exports = changeCase;